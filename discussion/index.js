// Array Methods - JavaScript as built-in functions and methods for arrays. This allows us to manipulate and access array items.

// ******Mutator Methods******
/*
	-Mutator methods are functions that "mutate" or change an aray after they're created.
	-These methods manipulate the original array performing various task such as adding and removing elements.
*/

// ******push()******
/*
	-ADDS an element in the end of an array and return the array's length.

	Syntax:

		arrayName.push()
*/

let myPets = ["Nana", "Sheldon", "Granger", "Bruno", "Cheshire"];

console.log("Current Array");
console.log(myPets);

let myPetslength = myPets.push("Dwarfino");
console.log(myPetslength);
	//  -return the number of elements in the array

console.log("Mutated array from push method");
console.log(myPets);

// Adding multiple elements to an array
myPets.push("Claude", "Gray");
console.log("Mutated array from push method");
console.log(myPets);

// ==================================================

// ******pop()******
/*
	-REMOVES the last element in an array and returns the removed element.

	Syntax:

		arrayName.pop()
*/

let removedmyPets = myPets.pop();
console.log(removedmyPets);
	// -return the removed element from the array

console.log("Mutated array from pop method");
console.log(myPets);

// ==================================================

// ******unshift()******
/*
	- ADDS one or more elements at the BEGINNING of an array

	Syntax:

		arrayName.unshift("elementA")
		arrayName.unshift("elementA", "ElementB")
*/

myPets.unshift("Unggoy", "Macy");
console.log("Mutated array from unshift method");
console.log(myPets);


// ==================================================

// ******shift()******

/*
	- REMOVES an element at the BEGINNING of an array and Returns the removed element

	Syntax:

		arrayName.shift()
*/

let anothermyPets = myPets.shift();
console.log(myPets);
console.log("Mutated array from shift method");
console.log(myPets);

// =================================================

// ******splice()******
/*
	- Simultaneously removes elements from a specified index number and adds element

	Syntax:

		arrayName.splice(startingIndex,
		deleteCount, elementsToBeAdded)
*/

console.log("Original Array:");
console.log(myPets);

// remove WITHOUT replacing
myPets.splice(1, 2);
console.log("Mutated array from splice method:");
console.log("remove WITHOUT replacing :");
console.log(myPets);

// remove WITH replacing
myPets.splice(1,2, "Galis", "Escanor");
console.log("remove WITH replacing");
console.log(myPets);


// Adding WITHOUT removing
myPets.splice(1,0, "Nana", "Sheldon");
console.log("Adding WITHOUT removing");
console.log(myPets);


// =================================================
// ***sort()***

/*
	-rearrange the array elemtns in alphanumeric order

	Syntax:

		arrayName.sort()
*/

// ex
myPets.sort();
console.log("Mutated array from sort method:");
console.log(myPets);


// ex
const animals = ["dog", "cat", "horse", "snake", "zebra"]
animals.sort()
console.log(animals)


// ***reverse()***

/*
	-reverse the order of arrat elements

	Syntax:

		arrayName.reverse();
*/

animals.reverse();
console.log("Mutated array from reverse method:");
console.log(animals)



// =================================================

// ****NON-MUTATOR METHOD****
/*
	- Non-mutator methods are functions that DO NOT MODIFY or CHANGE an array after they're created.

	- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output.
*/

// ***indexOf()****

/*
	returns the index number of the first matching elemtn found in an array.

	- if not match was found, the result will be 
	-1
	- the search process will be done from first element proceeding to the element

	Syntax:

		arrayName.indexOf(searchvalue)
*/

let letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i","j"]

let firstIndex = letters.indexOf("France")
console.log("Result of indexOf method: " + firstIndex);

// ******slice()******
/*
	- portion/slices elements from an array AND returns a new array.

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the first element

let sliceArrayA = letters.slice(4)
console.log("Original Array:")
console.log(letters)

console.log("Result from slice method: ")
console.log(sliceArrayA) 

console.log('Original array will not be affected by ".slice"')
console.log(letters)

// =============================================
// Slicing off elements from a specified index to another index
//  Note: the last element is not included

let sliceArrayB = letters.slice(0,3)
console.log("Result from slice method:")
console.log(sliceArrayB)

// =============================================
// Slicing off elements starting from the last element of an array

let sliceArrayC = letters.slice(-3)
console.log("Result from slice method:")
console.log(sliceArrayC)

// =============================================
// ******forEach()******

/*
	- similar to a for loop that iterates array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice
	- parameter - singular
	- Note: it must have function.

	Syntax:

		arrayName.forEach(function(indivElement)){
			statement
		}
*/

letters.forEach(function(letter) {
	console.log(letter)
})

// ==============================================
//  ******includes()******
/*
	- includes() method checks if the argument can be found in the array.
	- returns a boolean which can be saved in a variable.
	- return true if the argument is found in the array
	- return false if it's not
	Syntax:

		arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false

